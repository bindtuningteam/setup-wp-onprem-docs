### Platform version
- SharePoint 2019

### Browsers

BindTuning Web Part(s) work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Internet Explorer 11
- Edge

### SharePoint requirements 

- **SharePoint 2019** with a configured **App Catalog**. Please have a look at the <a href="https://support.bindtuning.com/hc/en-us/articles/360027696532" target="_blank">next article</a> to activate this option.
- Access to **App Catalog** with Administration rights.
- An active **BindTuning Web Parts** subscription or trial.