### Uninstall Add-in on APP catalog 

<p class="alert-success">The steps in this section need to be done only <strong>once per SharePoint instance</strong> by a global <strong>SharePoint Administrator</strong>. After this initial, the web parts will become unavailable for installation in each site collection.</p> 

---
#### SharePoint On-Premises

1. Open your **Central Administration** page;
1. Click on **Apps**; 

	![central-admin-app-catalog](../../../images/central-admin-app-catalog.png)

1. Under **App Management** select **Manage App Catalog**; 

1. Follow the link to your app catalog; 

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)
	
	
1. Now select **BT*xxxx* Add-in** that you want to delete and click on the ribbon **Files**;

1. Click to **Delete Document**;

	![Delete Document.PNG](https://bitbucket.org/repo/daEeqRX/images/3214804524-Delete%20Document.PNG)


-------
### Uninstall the Web Part on a Site Collection

<p class="alert-success">The steps in this section need to be done for <strong>each site collection</strong> where you want to delete the web part by a <strong>Site Collection Administrator</strong>.</p>

1. Inside your site collection, click on **Settings** ⚙️ and then on **Site contents**; 

	![Site Contents.PNG](https://bitbucket.org/repo/6499g4B/images/1722948873-Site%20Contents.PNG)
	
2. Click on the ellipsis button on the APP and then **Remove**; 
3. The Web Part will be removed from your SharePoint site.

	![RemoveAPP.png](https://bitbucket.org/repo/daEeqRX/images/2835600817-RemoveAPP.png)