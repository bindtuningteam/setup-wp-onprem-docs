If you want to use the SharePoint **Add-in** model to install the web part on your website, then you are in the right place. 😉

### Install Add-in on APP catalog 

<p class="alert-success">The steps in this section need to be done only <strong>once per SharePoint instance</strong> by a global <strong>SharePoint Administrator</strong>. After this initial installation, the web parts will become available for installation in each site collection.</p> 

---
#### SharePoint On-Premises

1. Open your **Central Administration** page;
1. Click on **Apps**; 

	![central-admin-app-catalog](../../../images/central-admin-app-catalog.png)
1. Under **App Management** select **Manage App Catalog**; 
1. Follow the link to your app catalog; 
1. Click on **Apps for SharePoint**; 

	![apps-for-sharepoint.png](../../../images/apps-for-sharepoint.png)

1. Now click on **Upload** and upload the app file that is inside your web part package;

	![Install.gif](https://bitbucket.org/repo/daEeqRX/images/2576993045-Install.gif)

Now you can install the  web part in every site collection in your SharePoint tenant. And that's what you are going to do next! 🙂

-------
### Install the Web Part on Site Collection

<p class="alert-success">The steps in this section need to be done for <strong>each site collection</strong> where you want to use the web part by a <strong>Site Collection Administrator</strong>.</p>

1. Open the Site Collection where you want your web part installed, and select **Add an App**;

	![Add an APP.PNG](https://bitbucket.org/repo/6499g4B/images/4002785227-Add%20an%20APP.PNG)
		
2. Search for **BT** in the search box and select the web part app;

	![Search_BT.PNG](https://bitbucket.org/repo/daEeqRX/images/3446996563-Search_BT.PNG)

3. Click on the **Trust It** button and wait for the app to install - you might need to wait for a few minutes;

	![Trust_IT.PNG](https://bitbucket.org/repo/daEeqRX/images/3669985873-Trust_IT.PNG)

4. Access your **Site Contents**. The installation will proceed **automatically**.

5. After the installation has finished, you see the added **BindTuning Add-ins** in a bright blue color.

	![add-in-installed.png](../../../images/add-in-installed.png)

Web part installed! ✅

-------

Done! To complete the setup process the only thing left to do is to add it to the page and configure all its properties.