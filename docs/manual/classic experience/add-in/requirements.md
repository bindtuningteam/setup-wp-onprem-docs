### Platform version
- SharePoint 2013
- SharePoint 2016
- SharePoint 2019

-------
### Browsers

BindTuning Web Part(s) work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Internet Explorer 11
- Edge

-------
### SharePoint requirements 

- **SharePoint 2013/2016/2019 On-premises** with **App Catalog**. Please have a look at the <a href="https://blogs.technet.microsoft.com/mspfe/2013/01/31/how-to-configure-sharepoint-2013-on- premises-deployments-for-apps/" target="_blank">next article</a> to activate/configure this option;
- Access to **App Catalog** with Administration rights;
- **Site Collection admin** rights;
- An active **BindTuning Web Parts** subscription or trial.