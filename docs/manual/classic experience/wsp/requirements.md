### Platform version
- SharePoint 2013
- SharePoint 2016
- SharePoint 2019

### Browsers

BindTuning Web Part(s) work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Internet Explorer 11
- Edge

### SharePoint requirements 

- You need to activate the **Custom Script** to see the Solutions. Please have a look at the <a href="https://support.bindtuning.pt/hc/en-us/articles/115003300926" target="_blank">next article</a> to activate this option;
- An active **BindTuning Web Parts** subscription or trial;
- Site Collection admin rights.