The Provisioning desktop application allows for easy deployment of BindTuning products for both SharePoint **On-premises** and **Office365** (SharePoint Online).

---
#### How to Download?

1. After logging into your BindTuning account, you'll be redirected to our app, where you'll be able to manage your products and subscriptions. <br>
On your homepage you'll have the option to download our **Provisioning Engine desktop app**.

    ![download-provisioning.png](../../images/download-provisioning.png)

1. After downloading, extract the contents of the **.zip** folder and run the application *ProvisioningEngineInstaller*.

1. After the installation is completed, you'll be able to utilize the **Provisiong Engine** to install BindTuning products. ✅