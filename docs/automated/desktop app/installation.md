**Note:** If you've already downloaded our **Provisioning Engine desktop app**, follow the instructions below; if not, follow the instructions <a href="https://bindtuning-setup-on-premises-web-part-guide.readthedocs.io/en/latest/automated/desktop%20app/download/" target="_blank">here</a>.

---
#### How to Install? 

**Note:** The **Provisioning Engine desktop application** will only display the themes that have been generated on your BindTuning account.

1. Open the desktop **Provisioning Engine**;

    ![provisioning-homescreen](../../images/provisioning-homescreen.png)

1. Login using your BindTuning account; 

    **Note:** Make sure to toggle the  **SharePoint** option.

    ![change-SP-version.gif](../../images/change-SP-version.gif)

     - Select your **SharePoint version**;
     -  Input either your **Tenant** or the particular **Site Collection** you want to install the Web Parts to;
    - Enter your credentials.

1. Navigate to the **Install** tab and change to the **Build** view;

 
1. Select the Web Part(s) you intend to install and click on Review&Install.
   
   ![change-to-build-provisioning.png](../../images/change-to-build-provisioning.png)

1. Review the products to be installed and click **Install**.

1. Choose the target for the installation:

    - **Classic** SharePoint experience;
    - **Modern** SharePoint experience and, subsequently, if you want to deploy the product on your **Tenant** or specific **Site Collection**.

    ![provisioning-choose-deployment.png](../../images/provisioning-choose-deployment.png)

1. Review the installation, click on **Accept and procced** and click **Install**.

1. The installation process will begin, immediately, and you'll be able to check its status, as well as to report any errors during the installation.

    ![provisioning-installation-process.png](../../images/provisioning-installation-process.png)

You're done! ✅

