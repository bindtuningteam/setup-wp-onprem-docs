BindTuning provides you with the flexibility to install our products the way you want to, regardless of technical knowledge or experience.

Following this, Bindtuning offers two main installation processes:

- **Automated** installation;
- **Manual** installation.

**Note:** We recommend the **Automated** installation, due to its easy deployment process.

---
### Automated Installation

The **Automated** installation process makes the deployment of BindTuning's products as easy as a simple click. Depending on your needs, you'll be able to choose how to proceed with the installation.

For **SharePoint On-premises**, Bindtuning offers one way to automatically deploy our products:

- **Desktop app** (former Provisioning Engine/ Clutch) - for both **Office 365** and **SharePoint On-premises**.

#### Desktop App
The **Provisioning Engine** will automatically deploy your Products, to both **SharePoint Online** and **SharePoint On-premises**.

<a href="https://bindtuning-setup-on-premises-web-part-guide.readthedocs.io/en/latest/automated/desktop%20app/installation/">Here</a> you'll learn how to install the products using the <strong>Provisioning desktop app</strong>.

---
### Manual installation
The **Manual** installation process allows for more granular control on the installation with an added complexity layer, not present in the **Automated** installation.

The **Manual** installation process will depend on the particular experience present on your site:

- For the **Classic** experience:
    * The installation process for the **Add-in** solution can be found <a href="https://bindtuning-setup-on-premises-web-part-guide.readthedocs.io/en/latest/manual/classic%20experience/add-in/requirements/">here</a>;
    * The installation process for the **WSP** solution can be found <a href="https://bindtuning-setup-on-premises-web-part-guide.readthedocs.io/en/latest/manual/classic%20experience/wsp/requirements/">here</a>.

- For the **Modern** experience, the installation process can be found <a href="https://bindtuning-setup-on-premises-web-part-guide.readthedocs.io/en/latest/manual/modern%20experience/requirements/">here</a>.

