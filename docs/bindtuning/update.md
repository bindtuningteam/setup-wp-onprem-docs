To update the BindTuning Web Parts on your SharePoint site you'll, first, need to update them on your BindTuning account.

1. Login on your **BindTuning account** at <a href="https://bindtuning.com/">BindTuning</a>;
1. Navigate to your **Build** tab;
1. If an update for the Web Part(s) is available, you'll be able to see a red circle.

    ![available-updates.png](../images/available-updates.png)

1. To update the Web Parts, click on the icon and select **Update Now**;

    ![update-one-webpart.png](../images/update-one-webpart.png)

1. The update process will begin and you'll be notified once the update has finished. ✅

